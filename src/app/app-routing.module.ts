import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PatientComponent} from "./patient/patient.component";
import {CaregiverComponent} from "./caregiver/caregiver.component";
import {DoctorComponent} from "./doctor/doctor.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'patients', component: PatientComponent },
  { path: 'caregivers', component: CaregiverComponent },
  { path: 'doctors', component: DoctorComponent },
  { path: '**', pathMatch: 'full', redirectTo: '/home' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
