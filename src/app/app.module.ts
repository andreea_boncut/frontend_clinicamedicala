import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { NavbarComponent } from './navbar/navbar.component';
import {PatientModule} from "./patient/patient.module";
import {CaregiverModule} from "./caregiver/caregiver.module";
import {DoctorModule} from "./doctor/doctor.module";
import {FooterComponent} from "./footer/footer.component";

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    PatientModule,
    CaregiverModule,
    DoctorModule,
    HttpClientModule
  ],
  declarations: [AppComponent, NavbarComponent, FooterComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
