import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Caregiver } from './caregiver';

@Injectable({
  providedIn: 'root'
})

export class CaregiverService {

  private baseUrl = "http://localhost:8080/caregivers";


  constructor(private http: HttpClient) { }

  getCaregivers(): Observable<Caregiver[]> {
    return this.http.get<Caregiver[]>(`${this.baseUrl}/`, {
      params: {
        caregivers: 'true'
      }
    });
  }

  deleteCaregiver(caregiverId: string): Observable<any>{
    console.log(caregiverId);
    return this.http.delete<any>(`${this.baseUrl}/`+caregiverId
    );
  }




}
