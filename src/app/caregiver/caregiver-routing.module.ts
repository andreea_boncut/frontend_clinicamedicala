import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CaregiverComponent } from './caregiver.component';


const routes: Routes = [{ path: 'caregivers', component: CaregiverComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaregiverRoutingModule {
  static components = [ CaregiverComponent ];
}
