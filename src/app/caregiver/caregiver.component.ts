import { Component, OnInit } from '@angular/core';
import {CaregiverService} from '../caregiver.service'
import { Caregiver } from '../caregiver';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  caregivers: Caregiver[] = [];
  constructor(private caregiverService: CaregiverService,  private toastr: ToastrService) { }

  ngOnInit(): void {
    this.caregiverService.getCaregivers().subscribe((data: Caregiver[]) => {
      console.log(data);
      this.caregivers = data;

    });
  }

  deleteCaregiver(caregiverId: string): void{
    this.caregiverService.deleteCaregiver(caregiverId)
      .subscribe(
        data => {
          this.toastr.success("Succes", "Caregiver deleted");

          this.loadCaregivers();

        },
        error => {
          this.toastr.error("Error", "Caregiver could not be deleted. Try again");
        });
  }

  loadCaregivers(): void{
    this.caregiverService.getCaregivers().subscribe((data: Caregiver[]) => {
      console.log(data);
      this.caregivers = data;})
  }

}
