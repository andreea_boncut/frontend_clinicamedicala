import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaregiverRoutingModule } from './caregiver-routing.module';
import {BrowserModule} from "@angular/platform-browser";
import {ToastrModule} from "ngx-toastr";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  imports: [CaregiverRoutingModule,  BrowserModule,BrowserAnimationsModule,ReactiveFormsModule,ToastrModule.forRoot({
    positionClass :'toast-bottom-right'
  })],
  declarations: [CaregiverRoutingModule.components]
})
export class CaregiverModule { }
