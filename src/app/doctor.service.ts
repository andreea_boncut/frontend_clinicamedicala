import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Doctor } from './doctor';

@Injectable({
  providedIn: 'root'
})

export class DoctorService {

  private baseUrl = "http://localhost:8080/doctors";


  constructor(private http: HttpClient) { }

  getDoctors(): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(`${this.baseUrl}/`, {
      params: {
        doctors: 'true'
      }
    });
  }

  deleteDoctor(doctorId: string): Observable<any>{
    console.log(doctorId);
    return this.http.delete<any>(`${this.baseUrl}/`+doctorId
    );
  }




}
