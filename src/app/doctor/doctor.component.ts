import { Component, OnInit } from '@angular/core';
import {DoctorService} from '../doctor.service'
import { Doctor } from '../doctor';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  doctors: Doctor[] = [];
  constructor(private doctorService: DoctorService,  private toastr: ToastrService) { }

  ngOnInit(): void {
    this.doctorService.getDoctors().subscribe((data: Doctor[]) => {
      console.log(data);
      this.doctors = data;

    });
  }

  deleteDoctor(doctorId: string): void{
    this.doctorService.deleteDoctor(doctorId)
      .subscribe(
        data => {
          this.toastr.success("Succes", "Doctor deleted");

          this.loadDoctors();

        },
        error => {
          this.toastr.error("Error", "Caregiver could not be deleted. Try again");
        });
  }

  loadDoctors(): void{
    this.doctorService.getDoctors().subscribe((data: Doctor[]) => {
      console.log(data);
      this.doctors = data;})
  }

}
