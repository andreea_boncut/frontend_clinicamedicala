import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DoctorRoutingModule } from './doctor-routing.module';
import {BrowserModule} from "@angular/platform-browser";
import {ToastrModule} from "ngx-toastr";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  imports: [DoctorRoutingModule,  BrowserModule,BrowserAnimationsModule,ReactiveFormsModule,ToastrModule.forRoot({
    positionClass :'toast-bottom-right'
  })],
  declarations: [DoctorRoutingModule.components]
})
export class DoctorModule { }
