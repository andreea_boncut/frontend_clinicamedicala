import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Patient } from './patient';

@Injectable({
  providedIn: 'root'
})

export class PatientService {

  private baseUrl = "http://localhost:8080/patients";


  constructor(private http: HttpClient) { }

  getPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(`${this.baseUrl}/`, {
      params: {
        patients: 'true'
      }
    });
  }

  deletePatient(patientId: string): Observable<any>{
    console.log(patientId);
    return this.http.delete<any>(`${this.baseUrl}/`+patientId
    );
  }




}
