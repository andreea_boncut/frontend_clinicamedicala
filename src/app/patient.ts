export class Patient {
  id: number =0;
  first_name: string="";
  last_name: string="";
  email: string="";
  medication: string="";
}
