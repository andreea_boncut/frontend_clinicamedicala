import { Component, OnInit } from '@angular/core';
import {PatientService} from '../patient.service'
import { Patient } from '../patient';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  patients: Patient[] = [];
  constructor(private patientService: PatientService,  private toastr: ToastrService) { }

  ngOnInit(): void {
    this.patientService.getPatients().subscribe((data: Patient[]) => {
      console.log(data);
      this.patients = data;

    });
  }

  deletePatient(patientId: string): void{
    this.patientService.deletePatient(patientId)
      .subscribe(
        data => {
          this.toastr.success("Succes", "Patient deleted");

          this.loadPatients();

        },
        error => {
          this.toastr.error("Error", "Patient could not be deleted. Try again");
        });
  }

  loadPatients(): void{
    this.patientService.getPatients().subscribe((data: Patient[]) => {
      console.log(data);
      this.patients = data;})
  }

}
