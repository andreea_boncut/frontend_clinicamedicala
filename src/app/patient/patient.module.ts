import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PatientRoutingModule } from './patient-routing.module';
import {BrowserModule} from "@angular/platform-browser";
import {ToastrModule} from "ngx-toastr";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  imports: [PatientRoutingModule,  BrowserModule,BrowserAnimationsModule,ReactiveFormsModule,ToastrModule.forRoot({
    positionClass :'toast-bottom-right'
  })],
  declarations: [PatientRoutingModule.components]
})
export class PatientModule { }
